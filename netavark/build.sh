#!/bin/sh

PKG_ROOT="$(pwd)/netavark_${NETAVARK_VERSION}-1"

mkdir -p $PKG_ROOT/usr/local/libexec/podman

git clone https://github.com/containers/netavark
cd netavark
git checkout "v${NETAVARK_VERSION}"
make DESTDIR=$PKG_ROOT
make DESTDIR=$PKG_ROOT docs
make DESTDIR=$PKG_ROOT install

cd ..
mkdir -p "${PKG_ROOT}/DEBIAN"
envsubst < netavark-control > "${PKG_ROOT}/DEBIAN/control"
dpkg-deb --build ${PKG_ROOT}

cp -f *.deb /packages