#!/bin/sh

PKG_ROOT="$(pwd)/aardvark_${AARDVARK_VERSION}-1"

mkdir -p $PKG_ROOT/usr/local/libexec/podman

git clone https://github.com/containers/aardvark-dns
cd aardvark-dns
git checkout "v${AARDVARK_VERSION}"
make DESTDIR=$PKG_ROOT
make DESTDIR=$PKG_ROOT install

cd ..
mkdir -p "${PKG_ROOT}/DEBIAN"
envsubst < aardvark-control > "${PKG_ROOT}/DEBIAN/control"
dpkg-deb --build ${PKG_ROOT}

cp -f *.deb /packages