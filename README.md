# podman-deb

Debian packages for up-to-date Podman releases. View the [package registry](https://gitlab.com/benridley/podman-deb/-/packages) to download the Debian packages.

## Usage
Install the .deb files found in the package registry. The easiest way:
```bash
wget https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/runc_1.1.0-1.deb
wget https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/conmon_2.1.0-1.deb
wget https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/cni-plugins_1.1.0-1.deb
wget https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/podman_4.0.1-1.deb

apt install ./*.deb
```
## Project status
I created this because I wanted to have the latest releases of Podman available to use on Debian and Ubuntu. It's mostly for my own use and I'm not an experienced
Debian packager by any means. That being said, feel free to raise issues and I will try and address them.